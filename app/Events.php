<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'artist', 'title', 'event', 'detail', 'date', 'livestream', 'published', 'user_id'
  ];

  public function user()
{
    return $this->belongsTo('App\User');
}

  public function getStreamIframe()
  {
    $iframe = $this->livestream;
    switch ($this->livestreamprovider) {
      case 'Twitch':
        // code...
        $countNumbers = count(array_filter(str_split($this->livestream),'is_numeric'));
        if($countNumbers < 4)
          $newiFrame = '<iframe src="https://player.twitch.tv/?channel='.$this->livestream . '" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>';
        else
          $newiFrame = '<iframe src="https://player.twitch.tv/?autoplay=false&video='.$this->livestream . '" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>';
        return $newiFrame;
        break;
      case 'Youtube':
      $newiFrame = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'. $this->livestream .'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden"></iframe>';
      return $newiFrame;
        break;

      default:
        //Facebook
        $newiFrame = '<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fstreamshare%2Fvideos%2F'. $this->livestream .'&mute=0" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>';
        return $newiFrame;
          // code...
        break;
    }
  }


}
