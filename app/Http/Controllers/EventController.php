<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Datatables;
use App\Events;
use Carbon\Carbon;
use Twitch;
use Embed\Embed;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function index()
     {
        // dd($events);
         return view('events/index');
     }

     public function eventsPast()
     {
       // $events = Events::all();
       $events = Events::where('published', 1)->whereDate('date', '<', Carbon::today()->toDateString())->orderBy('date','DESC')->paginate(18);
       $data = [
         'headline'        => 'past streams',
         'events'          => $events,
       ];
       // dd($event);
       return View('events/grid')->with($data);
     }

     public function eventsPresent()
     {
       // $events = Events::all();
       $events = Events::where('published', 1)->where('date', '<', Carbon::now())->whereDate('date', '=', Carbon::today()->toDateString())->orderBy('date','DESC')->paginate(18);
       $data = [
         'headline'        => 'present streams',
         'events'          => $events,
       ];
       // dd($event);
       return View('events/grid')->with($data);
     }

     public function eventsLive()
     {
       $events_today = Events::where('published', 1)->where('date', '<', Carbon::now())->whereDate('date', '=', Carbon::today()->toDateString())->count();
       $event = Events::where('published', 1)->get()->random();
       $events = Events::where('published', 1)->get()->random(3);
       $hasLivestream = false;
       if($events_today >= 1) {
         $hasLivestream = true;
         $event = Events::where('published', 1)->where('date', '<', Carbon::now())->whereDate('date', '=', Carbon::today()->toDateString())->get()->random();
       }
       if($events_today >= 3) {
         $events = Events::where('published', 1)->where('date', '<', Carbon::now())->whereDate('date', '=', Carbon::today()->toDateString())->get()->random(3);
       }
       $data = [
         'hasLivestream'  => $hasLivestream,
         'event'          => $event,
         'events'          => $events,
       ];
       // dd($event);
       return View('events/show')->with($data);
       // $events = Events::all();
       $events = Events::where('published', 1)->whereDate('date', '=', Carbon::today()->toDateString())->paginate(18);
     }

     public function eventsFuture()
     {
       // $events = Events::all();
       $events = Events::where('published', 1)->where('date', '>', Carbon::now())->orderBy('date','ASC')->paginate(18);
       $data = [
         'headline'        => 'future streams',
         'events'          => $events,
       ];
       // dd($event);
       return View('events/grid')->with($data);
     }

     public function ajaxEvents()
     {
       $events = DB::table('events')->orderBy('id','desc')->select('*');
       return datatables()->of($events)
       ->addColumn('action', function($row) {
            if ($row->published) {
              return '<a href="/events/'. $row->id .'/publish" class="btn btn-success btn-block">Unpublish</a>
                       <a href="/events/'. $row->id .'" class="btn btn-primary btn-block">Open</a>
                       <a href="/events/'. $row->id .'/edit" class="btn btn-secondary btn-block">Edit</a>
                       <a onclick="return confirm(\'Are you sure you want to delete?\');" href="/events/'. $row->id .'/destroy" class="btn btn-danger btn-block">Delete</a>';
            }
            else {
              return '<a href="/events/'. $row->id .'/publish" class="btn btn-warning btn-block">Publish</a>
                        <a href="/events/'. $row->id .'" class="btn btn-secondary btn-block">Open</a>
                        <a href="/events/'. $row->id .'/edit" class="btn btn-secondary btn-block">Edit</a>
                        <a onclick="return confirm(\'Are you sure you want to delete?\');" href="/events/'. $row->id .'/destroy" class="btn btn-danger btn-block">Delete</a>';
            }
         })
         ->make(true);
     }

         /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('events/create');
    }
         /**
     * Show the form for editing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //Find the employee
      $event = Events::find($id);
      return view('events/edit',['event'=> $event]);
    }


    public function update(Request $request)
    {
      // dd($request);
      $event = Events::find($request->id);
      // dd($event);
      $event->artist = $request->artist;
      $event->event = $request->event;
      $event->detail = $request->detail;
      $event->slug = 'test';
      // $event->date = $request->date;
      // dd($request->timezone);
      // $event->date = Carbon::parse($request->date, $request->timezone);
      // $event->date = Carbon::parse($request->date, $request->timezone)->setTimezone('UTC');
      $event->date = Carbon::parse($request->date, 'Europe/Berlin')->setTimezone('UTC');
      if($request->image) {
        $event->image = $request->image;
      }
      $event->livestream = $request->livestream;
      $event->livestreamprovider = $request->livestreamprovider;
      $event->donation = $request->donation;
      $user = auth()->user();
      if($user == NULL) {
        $event->save();
      }
      else {
        $user->events()->save($event);
      }
      return redirect()->back()->with('success', 'Event has been updated!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $event = new Events();
      $event->artist = $request->artist;
      $event->event = $request->event;
      $event->detail = $request->detail;
      $event->slug = $request->date;
      // $event->date = Carbon::parse($request->date, $request->timezone)->setTimezone('UTC');
      $event->date = Carbon::parse($request->date, 'Europe/Berlin')->setTimezone('UTC');
      // $event->date = $request->date;
      $event->livestream = $request->livestream;
      if($request->image) {
        $event->image = $request->image;
      }
      $event->livestreamprovider = $request->livestreamprovider;
      $event->donation = $request->donation;
      // $event->location =
      $user = auth()->user();
      if($user == NULL) {
        $event->save();
      }
      else {
        $user->events()->save($event);
      }
      return redirect()->back()->with('success', 'Thank you! Your event has been submitted!');
    }

    public function show($id) {
      $event = Events::find($id);
      $events = Events::where('published', 1)->get()->random(3);
      $data = [
        'event'          => $event,
        'events'          => $events,
      ];
      // dd($event);
      return View('events/show')->with($data);
      // return redirect()->back()->with('success', 'Event has been created!');
    }

    public function showPublic($id) {
      $event = Events::find($id);
      $events = Events::where('published', 1)->get()->random(3);
      $data = [
        'event'          => $event,
        'events'          => $events,
      ];
      // dd($event);
      return View('events/show')->with($data);
      // return redirect()->back()->with('success', 'Event has been created!');
    }

    public function publish($id) {
      $event = Events::find($id);
      $event->published = !$event->published;
      $event->save();
      return redirect()->back()->with('success', 'Event has been published!');
      // return redirect()->back()->with('success', 'Event has been created!');
    }

    public function destroy($id)
    {
        //Retrieve the employee
        $event = Events::find($id);
        //delete
        $event->delete();
        return redirect()->back()->with('success', 'Event has been destroyed!');
    }

    public function checkTwitchStatus() {
      $count_prevEvents_offline = 0;
      $count_prevEvents_online = 0;
      $prev_events = Events::where('livestreamprovider', 'Twitch')->where('date', '<', Carbon::now())->get();
      foreach ($prev_events as $prev_event) {
        $countNumbers = count(array_filter(str_split($prev_event->livestream),'is_numeric'));
        //check if channel
        if($countNumbers < 4) {
          //get user_id
          $request = 'users';
          $user = Twitch::performGetRequest($request,['login' => $prev_event->livestream],[]);
          $user_id = $user->users[0]->_id;
          $request = 'streams/'.$user_id;
          // $request = 'streams/2343423';
          $channel = Twitch::performGetRequest($request);
          // if()
          // https://api.twitch.tv/kraken/users?login=dallas
          if($channel->stream == null) {
            if($prev_event->published) {
              $prev_event->published = false;
              $prev_event->save();
              $count_prevEvents_offline++;
            }
          }
          else {
            if(!$prev_event->published) {
              // dd($channel);
              // $prev_event->event = $channel->stream->channel->status;
              if($prev_event->event == null) {
                $prev_event->event = $channel->stream->channel->status;
              }
              if($prev_event->details == null) {
                $prev_event->detail = $channel->stream->channel->description;
              }
              $prev_event->published = true;
              // $prev_event->date = Carbon::now();

              $prev_event->date = Carbon::parse($channel->stream->created_at);
              $prev_event->save();
              $count_prevEvents_online++;
            }
          }
          // $channel = Twitch::performGetRequest($request,['game' => 'Music & Performing Arts','language' => 'de'],[]);
        }
      }
      return redirect()->back()->with('success', $count_prevEvents_offline . '  Twitch stream(s) set offline  '. $count_prevEvents_online . ' stream(s) set back to active.');
    }

    public function twitchapi()
    {
      // $channel = Twitch::getChannelById(44322889);
      $events = collect();
      // $events = Events::where('published', 1)->get()->random(3);
      $id = 44322889;
      $request = 'streams/';
      $channel = Twitch::performGetRequest($request,['game' => 'Music & Performing Arts','broadcaster_language' => 'de'],[]);
      // dd($channel);
      $count_newstreams = 0;
      $count_updatedstreams = 0;
      foreach ($channel->streams as $stream) {
        // $requests = 'streams/'.$stream->channel->_id;
        // $channels = Twitch::performGetRequest($requests);
        // dd($stream);
        //only get german streams
        if($stream->channel->broadcaster_language == "de") {
          $event = new Events();
          // dd($stream);
          $event->artist = $stream->channel->display_name;
          $event->event = $stream->channel->status;
          $event->image = $stream->preview->medium;
          $event->detail = $stream->channel->description;
          $event->slug = $stream->channel->_id;
          // $event->date = Carbon::now();
          // $event->date = Carbon::createFromFormat('Y-m-dTH:i:sZ',$stream->created_at, 'Europe/London');
          $event->date = Carbon::parse($stream->created_at);
          $event->published = true;
          $event->livestream = $stream->channel->name;
          $event->livestreamprovider = 'Twitch';
          //check if event has been saved before:
          $prev_events = Events::where('livestreamprovider', 'Twitch')->where('livestream',$stream->channel->name)->get();
          if($prev_events->count()>0) {
            //reactivate old twitch streams
            // foreach ($prev_events as $prev_event) {
            //   if($prev_event->published) {
            //     if($prev_event->details == null) {
            //       $prev_event->detail = $stream->channel->description;
            //       $prev_event->save();
            //     }
            //   }
            //   if($prev_event->published == false) {
            //     $prev_event->published = true;
            //     $prev_event->date = Carbon::now();
            //     if($prev_event->details == null) {
            //       $prev_event->detail = $stream->channel->description;
            //       $prev_event->save();
            //     }
            //     $count_updatedstreams++;
            //     $prev_event->save();
            //   }
            // }
          }
          else {
            $event->save();
            $events->add($event);
            $count_newstreams++;
          }
        }

      }
      $success = $count_newstreams . ' new Twitch streams have been added.';
      $data = [
        'headline'        => 'present streams',
        'events'          => $events,
        'success'         => $success,
      ];
      // dd($event);
      // return View('events/grid')->with($data);
      // return redirect()->back()->with('success', 'Thank you! Your event has been submitted!');
      // return redirect()->back()->with('success', $count_newstreams . ' new Twitch streams have been added.');
      return redirect()->back()->with($data);
      // return redirect()->back()->with('success', $count_newstreams . ' new Twitch streams have been added and ' . $count_updatedstreams . ' old streams have been reactivated!');
    }

    public function checkgetImages()
    {
      $info = Embed::create('https://www.facebook.com/onemuc/videos/500505360619428/');
      dd($info);
    }
}
