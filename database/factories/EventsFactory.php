<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Events;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(Events::class, function (Faker $faker) {

    return [
      'artist' => $faker->name,
      'event' => $faker->sentence(5),
      'detail' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
      'slug' => $faker->slug,
      'location' => $faker->city,
      'category' => $faker->randomElement([
         'Music',
         'Cooking',
         'Performance',
       ]),
      'date' => $faker->dateTimeBetween('last sunday', 'next saturday'),
      'livestream' => $faker->url,
      'livestreamprovider' => $faker->randomElement([
         'Youtube',
         'Twitch',
         'Facebook',
       ]),
      'donation' => $faker->randomElement([
         'https://www.paypal.de',
         'https://www.patreon.com',
       ]),
      'user_id' => 1,
      'published' => $faker->boolean,
      'img' => 'https://picsum.photos/200',
    ];
});
