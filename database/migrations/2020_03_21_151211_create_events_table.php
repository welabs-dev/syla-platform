<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('artist');
            $table->string('event')->nullable();
            $table->string('slug')->nullable();
            $table->string('location')->nullable();
            $table->string('category')->nullable();
            $table->text('detail')->nullable();
            $table->dateTime('date');
            $table->text('livestream')->nullable();
            $table->string('livestreamprovider')->nullable();
            $table->string('donation')->nullable();
            $table->string('img')->nullable();
            $table->boolean('published')->default(false);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
