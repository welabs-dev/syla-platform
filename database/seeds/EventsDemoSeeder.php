<?php

// use Illuminate\Database\Seeder;
use JeroenZwart\CsvSeeder\CsvSeeder;

class EventsDemoSeeder extends CsvSeeder
{
  public function __construct()
      {
          $this->file = '/database/seeds/csvs/events.csv';
      }

      /**
       * Run the database seeds.
       *
       * @return void
       */
      public function run()
      {
          // Recommended when importing larger CSVs
  	    DB::disableQueryLog();
  	    parent::run();
      }
}
