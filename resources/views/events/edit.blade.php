@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="">

          <div class="">

            <div class="container">
                {{-- <h1>Create a link</h1> --}}
                <div class="col-md-12">
                {{-- <div class="row"> --}}
                <form action="/events/update" method="post">
                  @if (session()->has('success'))
                      <div class="alert alert-success">
                          {{ session('success') }}
                      </div>
                  @endif
                  @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                      Please fix the following errors
                    </div>
                  @endif

                  {!! csrf_field() !!}
                  @method('PUT')
                  <div class="form-group{{ $errors->has('artist') ? ' has-error' : '' }}">
                    <label for="artist">Artist</label>

                    <input type="text" class="form-control" id="artist" name="artist" placeholder="Enter your artist name" value="{{ $event->artist }}">
                    @if($errors->has('artist'))
                      <span class="help-block">{{ $errors->first('artist') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('event') ? ' has-error' : '' }}">
                    <label for="event">Event</label>
                    <input type="text" class="form-control" id="event" name="event" placeholder="What is the name of your event" value="{{ $event->event }}">
                    @if($errors->has('event'))
                      <span class="help-block">{{ $errors->first('event') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('detail') ? ' has-error' : '' }}">
                    <label for="detail">Short Description</label>
                    <input type="text" class="form-control" id="detail" name="detail" placeholder="Give us some details" value="{{ $event->detail }}">
                    @if($errors->has('detail'))
                      <span class="help-block">{{ $errors->first('detail') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('livestream') ? ' has-error' : '' }}">
                    <label for="livestream">Livestream URL</label>
                    <input type="text" class="form-control" id="livestream" name="livestream" placeholder="Embed code of your Livestream" value="{{ $event->livestream }}">
                    @if($errors->has('livestream'))
                      <span class="help-block">{{ $errors->first('livestream') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('livestreamprovider') ? ' has-error' : '' }}">
                    <label for="livestreamprovider">Livestream Provider</label>
                    <input type="text" class="form-control" id="livestreamprovider" name="livestreamprovider" placeholder="Facebook / Youtube / Twitch" value="{{ $event->livestreamprovider }}">
                    @if($errors->has('livestreamprovider'))
                      <span class="help-block">{{ $errors->first('livestreamprovider') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image">Preview Image URL</label>
                    <input type="text" class="form-control" id="image" name="image" placeholder="Add the URL to your preview image" value="{{ $event->image }}">
                    @if($errors->has('image'))
                      <span class="help-block">{{ $errors->first('image') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="date">Date</label>
                    <input type="text" class="form-control" id="date" name="date" placeholder="Date" value="{{ $event->date }}">
                    @if($errors->has('date'))
                      <span class="help-block">{{ $errors->first('date') }}</span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('donation') ? ' has-error' : '' }}">
                    <label for="donation">Donation</label>
                    <input type="text" class="form-control" id="donation" name="donation" placeholder="Your Link to your donation page" value="{{ $event->donation }}">
                    @if($errors->has('donation'))
                      <span class="help-block">{{ $errors->first('donation') }}</span>
                    @endif
                  </div>
                  <input type="hidden" name="id" value = "{{$event->id}}">
                  <input type="hidden" name="timezone" id="timezone">
                  <button type="submit" class="btn btn-primary">Update</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('footer_scripts')
<script>
   $(document).ready( function () {
     var timezone = moment.tz.guess();
      // $('#date').val(timezone);
      var time = $('#date').val();
      var time_adjusted = moment.utc(time).tz(timezone).format('YYYY-MM-DD HH:mm:ss');
      // alert(time_adjusted);
      $('#date').val(time_adjusted);
     $.datetimepicker.setLocale('de');
     $('#date').datetimepicker();
   });
</script>
@endsection
