@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="container margin-bottom">

          {{-- <h1>Create a link</h1> --}}
          <div class="col-lg-12">
            <div>
              <h2>{{$headline}}</h2>
              {{-- <hr> --}}
            </div>
            {{-- @if(null !==$events->links())
              <div>{{ $events->links() }}</div>
            @endif --}}
            <div class="grid">
              @if($events->count() == 0)
                <h3>There are no {{$headline}} right now.</h3>
              @endif
              <div class="grid-sizer"></div>
              @foreach ($events as $event)
                <a href="/event/{{{$event->id}}}">
                  {{-- <div class="grid-item-home" style="background:linear-gradient(0deg,rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url(https://{{ $imgUrl }}?width=1280); min-height:{{$widgetHeight}}px; background-size:cover; background-repeat: no-repeat"> --}}
                  @if($event->image)
                    <div class="event-grid-items grid-item" style="background:linear-gradient(0deg,rgba(4, 192, 157, 0.71),rgba(4, 192, 157, 0.71)),url('{{$event->image}}');  background-size:cover; background-repeat: no-repeat">
                  @else
                    <div class="event-grid-items grid-item" style="background:linear-gradient(0deg,rgba(4, 192, 157, 0.71),rgba(4, 192, 157, 0.71)),url('/images/grid/grid_thumb_3.jpg');  background-size:cover; background-repeat: no-repeat">
                  @endif
                  {{-- <div class="event-grid-items grid-item" style="padding:10px; background-image: url('{{$event->image}}');"> --}}
                    <h3 class="event-grid-artist">{{$event->artist}}</h3>
                    <h4 class="event-grid-title">{{$event->event}}</h4>
                    <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                    {{-- <hr> --}}
                    <p class="event-grid-description">
                    {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                    {{-- <img src="{{$event->img}}" width="100%"> --}}
                    <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                  </div>
                </a>
              @endforeach
            </div>
            {{-- @if(null !==$events->links())
              <div>{{ $events->links() }}</div>
            @endif --}}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('footer_scripts')
<script>
   $(document).ready( function () {
     var timezone = moment.tz.guess();
      $('#timezone').val(timezone);
      var $grid = $('.grid').masonry({
        columnWidth: '.grid-sizer',
        itemSelector: '.grid-item',
        percentPosition: true,
        gutter: 0
      });
      // layout Masonry after each image loads
      $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
      });
   });
</script>
@endsection
