@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="container margin-bottom">

{{-- Past --}}

          {{-- <h1>Create a link</h1> --}}
          <div class="col-lg-12 section_past">
            <div>
              <h3>Past Streams</h3>

            </div>
            <div class="grid">
              @foreach ($events as $event)
                <a href="/events/{{{$event->id}}}">
                  <div class="event-grid-items grid-item" style="padding:10px; width: 30%; min-height: 150px; margin-bottom: 10px;">
                    <h3>{{$event->artist}}</h3>
                    <h4>{{$event->event}}</h4>
                    <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                    <p class="event-grid-description">
                    {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                    {{-- <img src="{{$event->img}}" width="100%"> --}}
                    <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                  </div>
                </a>
              @endforeach
            </div>
            <div>{{ $events->links() }}</div>
          </div>

{{-- Present --}}

          <div class="col-lg-12 section_present">
            <div>
              <h3>Present Streams</h3>

            </div>
            <div class="grid">
              @foreach ($events as $event)
                <a href="/events/{{{$event->id}}}">
                  <div class="event-grid-items grid-item" style="padding:10px; width: 30%; min-height: 150px; margin-bottom: 10px;">
                    <h3>{{$event->artist}}</h3>
                    <h4>{{$event->event}}</h4>
                    <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                    <p class="event-grid-description">
                    {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                    {{-- <img src="{{$event->img}}" width="100%"> --}}
                    <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                  </div>
                </a>
              @endforeach
            </div>
            <div>{{ $events->links() }}</div>
          </div>


{{-- Future --}}

          <div class="col-lg-12 section_future">
            <div>
              <h3>Future Streams</h3>

            </div>
            <div class="grid">
              @foreach ($events as $event)
                <a href="/event/{{{$event->id}}}">
                  <div class="event-grid-items grid-item event-grid-bg-1" style="padding:10px; width: 30%; min-height: 150px; margin-bottom: 10px;">
                    <h3>{{$event->artist}}</h3>
                    <h4>{{$event->event}}</h4>
                    <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                    <p class="event-grid-description">
                    {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                    {{-- <img src="{{$event->img}}" width="100%"> --}}
                    <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                  </div>
                </a>
              @endforeach
            </div>
            <div>{{ $events->links() }}</div>
          </div>


        </div>
      </div>
    </div>
  </div>
@endsection
@section('footer_scripts')
<script>
   $(document).ready( function () {
      var $grid = $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        columnWidth: 33
      });
      // layout Masonry after each image loads
      $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
      });
   });
</script>
@endsection
