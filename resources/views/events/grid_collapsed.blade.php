@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="container margin-bottom">

          {{-- <h1>Create a link</h1> --}}
          <div class="col-lg-12">
            <div>
              <h3>{{$headline}}</h3>

            </div>

            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Tomorrow
                    </button>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                    <div class="grid">
                      @foreach ($events as $event)
                        <a href="/event/{{{$event->id}}}">
                          <div class="event-grid-items col-3" style="padding:10px; width: 30%; min-height: 150px; margin-bottom: 10px;">
                            <h3>{{$event->artist}}</h3>
                            <h4>{{$event->event}}</h4>
                            <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                            <p class="event-grid-description">
                              {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                              {{-- <img src="{{$event->img}}" width="100%"> --}}
                              <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                            </div>
                          </a>
                        @endforeach
                      </div>
                      <div>{{ $events->links() }}</div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Next week
                      </button>
                    </h5>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                      <div class="grid">
                        @foreach ($events as $event)
                          <a href="/event/{{{$event->id}}}">
                            <div class="event-grid-items col-3" style="padding:10px; width: 30%; min-height: 150px; margin-bottom: 10px;">
                              <h3>{{$event->artist}}</h3>
                              <h4>{{$event->event}}</h4>
                              <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                              <p class="event-grid-description">
                                {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                                {{-- <img src="{{$event->img}}" width="100%"> --}}
                                <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                              </div>
                            </a>
                          @endforeach
                        </div>
                        <div>{{ $events->links() }}</div>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headingThree">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Later
                        </button>
                      </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                        <div class="grid">
                          @foreach ($events as $event)
                            <a href="/event/{{{$event->id}}}">
                              <div class="event-grid-items col-3" style="padding:10px; width: 25%; min-height: 150px; margin-bottom: 10px;">
                                <h3>{{$event->artist}}</h3>
                                <h4>{{$event->event}}</h4>
                                <p class="event-grid-date">{{ \Carbon\Carbon::parse($event->date)->diffForHumans() }}</p>
                                <p class="event-grid-description">
                                  {{ \Illuminate\Support\Str::limit($event->detail, 50, $end='...') }}</p>
                                  {{-- <img src="{{$event->img}}" width="100%"> --}}
                                  <span class="badge badge-{{$event->livestreamprovider}}">{{$event->livestreamprovider}}</span>
                                </div>
                              </a>
                            @endforeach
                          </div>
                          <div>{{ $events->links() }}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endsection
      @section('footer_scripts')
        {{-- <script>
        $(document).ready( function () {
          var $grid = $('.grid').masonry({
            // options
            itemSelector: '.grid-item',
            columnWidth: 33
          });
          // layout Masonry after each image loads
          $grid.imagesLoaded().progress( function() {
            $grid.masonry('layout');
          });
        });
      </script> --}}
    @endsection
