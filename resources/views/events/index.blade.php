@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
          @if (session()->has('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
            <div class="">
              <div class="btn-group float-right" role="group" aria-label="Basic example">
                <a  style="margin:5px" onclick="return confirm('Do you want to start the Twitch import process?');" href="/twitch" class=""><button type="button" class="btn btn-success">Import from Twitch</button></a>
                <a  style="margin:5px" onclick="return confirm('Do you want to start the Twitch status check process?');" href="/checktwitchstatus" class=""><button type="button" class="btn btn-success">Check status of all Twitch streams</button></a>
                <a href="/events/create"  style="margin:5px" class=""><button type="button" class="btn btn-primary">Create Event</button></a>
              </div>
              <hr>
                    <div class="">Events</div>
                    <br>

                <div class="">
                  <table class="table table-bordered" id="laravel_datatable">
                     <thead>
                        <tr>
                           <th>Id</th>
                           <th>Artist</th>
                           <th>Event</th>
                           <th>Detail</th>
                           <th>Date</th>
                           <th>Platform</th>
                           <th>Livestream</th>
                           <th>Donation</th>
                           {{-- <th>Published</th> --}}
                           <th>Action</th>
                        </tr>
                     </thead>
                  </table>

                </div>
              </div>
            </div>
          </div>
        </div>
      @endsection
      @section('footer_scripts')
      <script>
         $(document).ready( function () {

           // alert('hello');
          $('#laravel_datatable').DataTable({
                 processing: true,
                 serverSide: true,
                 responsive: true,
                 ajax: "{{ url('ajaxEvents') }}",
                 columns: [
                          { data: 'id', name: 'id' },
                          { data: 'artist', name: 'Artist' },
                          { data: 'event', name: 'Event' },
                          { data: 'detail', name: 'Detail' },
                          { data: 'date', name: 'Date',"render": function ( data, type, row, meta ) {
                            var timezone = moment.tz.guess();
                            return moment.utc(data).tz(timezone).format('YYYY-MM-DD HH:mm:ss');
                          }},
                          { data: 'livestreamprovider', name: 'Platform', orderable: false, searchable: false },
                          { data: 'livestream', name: 'Livestream' },
                          { data: 'donation', name: 'Donation' },
                          // { data: 'published', name: 'Published' },
                          { data: 'action', name: 'Action', orderable: false, searchable: false}
                       ]
              });
           });
        </script>
    @endsection
