@extends('layouts.app')

@section('content')
  <div class="container padding-containter-bottom">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="">

          <div class="">

            <div class="container margin-container-bottom">

              {{-- <h1>Create a link</h1> --}}
              @isset($hasLivestream)
                @if ($hasLivestream)
                  <h3>Check out what is live right now</h3>
                @else
                  <h3>There are no livestreams available right now. Check out our other videos:</h3>
                @endif
                <hr>
              @endisset

              <h1>{{$event->artist}}</h1>
              <div class="row">
                <div class="col-md-8 col-xs-12">
                  <div style="padding-top:66.25%;background-color: #f8fafc;">
                    {{-- <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftbsessionband%2Fvideos%2F506258290047427&show_text=1&width=560&mute=0&autoplay=0" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe> --}}
                    {{-- <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftbsessionband%2Fvideos%2F506258290047427&show_text=1&width=560&mute=0&autoplay=1" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe> --}}
                    @if($event->livestream)
                      {!!$event->getStreamIframe()!!}
                      {{-- <iframe src="https://player.twitch.tv/?channel=tarayah" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe> --}}
                    @endif
                    {{-- <iframe src="https://player.twitch.tv/?channel=tarayah" width="100%" height="100%" style="position:absolute;top:0;left:0;width:100%;height:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe> --}}
                  </div>
                </div>
                <div class="col">
                  <div>
                    <h2 class="event_artist">{{$event->event}}</h2>
                    <p class="event_date">Live on {{ \Carbon\Carbon::parse($event->date)->format('d.m.Y') }} at {{ \Carbon\Carbon::parse($event->date)->timezone('Europe/Berlin')->isoFormat('HH:mm') }}</p>
                  </div>
                  <div>
                    <p  class="event_title">{{$event->detail}}</p>
                  </div>
                  {{-- <div>
                    <p class="event_artist"><h2>Date</h2></p>
                    <p>{{$currevent->date}}</p>
                  </div> --}}
                  <div>
                    @if($event->donation != NULL)
                      <a class=" btn btn-primary theme_button  btn-lg btn-block " target="_blank" href="{{$event->donation}}">Donate</a>
                    @endif
                    <button class="linkcopybtn btn btn-primary theme_button  btn-lg btn-block " data-clipboard-text="https://www.streamshift.de/event/{{$event->id}}" >COPY LINK</a>
                  </div></div>
                </div>
              </div>

              <div class="col-lg-12 section_future">
                <div>
                  <h3>Past & Current Livestreams</h3>

                </div>
                <div class="grid">
                  <div class="grid-sizer"></div>
                  @foreach ($events as $otherevent)
                    <a href="/event/{{{$otherevent->id}}}">
                      @if($otherevent->image)
                      <div class="event-grid-items grid-item" style="background:linear-gradient(0deg,rgba(4, 192, 157, 0.71),rgba(4, 192, 157, 0.71)),url('{{$otherevent->image}}');  background-size:cover; background-repeat: no-repeat">
                      @else
                        <div class="event-grid-items grid-item" style="background:linear-gradient(0deg,rgba(4, 192, 157, 0.71),rgba(4, 192, 157, 0.71)),url('/images/grid/grid_thumb_3.jpg');  background-size:cover; background-repeat: no-repeat">
                      @endif
                      {{-- <div class="event-grid-items grid-item" style="padding:10px;  min-height: 250px;"> --}}
                        <h3 class="event-grid-artist">{{$otherevent->artist}}</h3>
                        <h4 class="event-grid-title">{{$otherevent->event}}</h4>
                        <p class="event-grid-date">{{ \Carbon\Carbon::parse($otherevent->date)->diffForHumans() }}</p>
                        <p class="event-grid-description">
                        {{ \Illuminate\Support\Str::limit($otherevent->detail, 50, $end='...') }}</p>
                        {{-- <img src="{{$event->img}}" width="100%"> --}}
                        <span class="badge badge-{{$otherevent->livestreamprovider}}">{{$otherevent->livestreamprovider}}</span>
                      </div>
                    </a>
                  @endforeach
                </div>
                <a class=" btn btn-primary theme_button  btn-lg" href="{{ route('events.present')  }}">Browse All Livestreams</a>
                {{-- <div>{{ $events->links() }}</div> --}}
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
@section('footer_scripts')
<script>
   $(document).ready( function () {
     new ClipboardJS('.linkcopybtn');
     // var timezone = moment.tz.guess();
     //  $('#timezone').val(timezone);
      var $grid = $('.grid').masonry({
        columnWidth: '.grid-sizer',
        itemSelector: '.grid-item',
        percentPosition: true,
        gutter: 0
      });
      // layout Masonry after each image loads
      $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
      });
   });
</script>
@endsection
