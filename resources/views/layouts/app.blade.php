<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="author" content="Streamshift">

        @if(!empty($event) && !Request::is('events/live'))
          <meta property="og:url"                content="{{Request::url()}}" />
          <meta property="og:type"               content="article" />
          <meta property="og:title"              content="{{$event->artist}}: {{$event->event}}" />
          <meta property="og:description"        content="{{$event->detail}}" />
          @if(!empty($event->image))
            <meta property="og:image"              content="{{$event->image}}" />
          @else
            <meta property="og:image"              content="https://streamshift.de/images/preview.jpg" />

          @endif
        @else
          <meta property="og:url"                content="{{Request::url()}}" />
          <meta property="og:type"               content="website" />
          <meta property="og:title"              content="Streamshift" />
          <meta property="og:description"        content="Die digitale Streaming-Plattform STREAMSHIFT bietet lokalen Künstler:innen die Möglichkeit, sich live und digital mit ihrem Publikum zu verbinden, unabhängig von der Streaming Plattform." />
          <meta property="og:image"              content="https://streamshift.de/images/preview.jpg" />
        @endif

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <link  href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.js"></script>

    <script src="{{ asset('js/clipboard.min.js') }}"></script>

    <script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
    <link  href="{{ asset('css/jquery.datetimepicker.min.css') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-161531725-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-161531725-1');
    </script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-theme fixed-top shadow-sm">
            <div class="container">
                <a class="navbar-brand navbar-brand-center" href="{{ url('/') }}">
                  <div class="navbar-logo-container">
                    <div class="navbar-logo">
                    </div>
                    {{-- <img width=220px src="{{ asset('images/streamshift.png') }}"> --}}
                  </div>
                    {{-- {{ config('app.name', 'Laravel') }} --}}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    {{-- <ul class="navbar-nav mr-auto">
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('events.past') }}">Past</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('events.present') }}">Present</a>
                      </li>
                      <li class="nav-item">
                        <a class=" nav-link" href="{{ route('events.future') }}">Future</a>
                      </li>
                    </ul> --}}

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                          <li class="nav-item">
                            <a class="nav-link" href="/events/create">Add Event</a>
                          </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li> --}}
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('events.index') }}">Manage Events</a>
                          </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        <li class="nav-item">
                          <a class="nav-link" href="/imprint">Imprint & Data Privacy</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4 margin-container-bottom margin-container-top">
            @yield('content')
        </main>
    </div>

    <!-- Footer -->
<footer class="footer-syla page-footer font-small fixed-bottom">
  @include('cookieConsent::index')
  <div class="container">
    <div class="row">
      <a  class="col-sm text-center footer-btn {{ (Request::is('events/past')) ? 'footer-btn-active' : '' }}" href="{{ route('events.past')  }}">
        Past</a>
      <a class="col-sm text-center footer-btn {{ (Request::is('events/live')) || (Request::is('events/present')) ? 'footer-btn-active' : '' }}" href="{{ route('events.live')  }}">
        Present</a>
      <a  class="col-sm text-center footer-btn {{ (Request::is('events/future')) ? 'footer-btn-active' : '' }}" href="{{ route('events.future')  }}">
        Future</a>
    </div>
  </div>

</footer>
<!-- Footer -->
        @yield('footer_scripts')
</body>
</html>
