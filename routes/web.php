<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('events.live'));
});

Route::view('/imprint', 'static.imprint');

Route::get('/events/past', [
  'as'        => 'events.past',
  'uses'      => 'EventController@eventsPast',
]);
Route::get('/events/present', [
  'as'        => 'events.present',
  'uses'      => 'EventController@eventsPresent',
]);

Route::get('/events/live', [
  'as'        => 'events.live',
  'uses'      => 'EventController@eventsLive',
]);

Route::get('/events/future', [
  'as'        => 'events.future',
  'uses'      => 'EventController@eventsFuture',
]);

Route::get('/events/all', [
  'as'        => 'events.all',
  'uses'      => 'EventController@eventsAll',
]);


Route::get('/event/{id}', [
  'as'        => 'events.showPublic',
  'uses'      => 'EventController@showPublic',
]);

// Route::resource('events','EventController');

Route::post('/events/store', [
  'as'        => 'storeEvent',
  'uses'      => 'EventController@store',
]);
Route::get('/events/create', [
  'as'        => 'createEvent',
  'uses'      => 'EventController@create',
]);





Route::group(['middleware' => ['auth']], function() {

  Route::get('/checkgetImages', [
    'uses'      => 'EventController@checkgetImages',
  ]);


  Route::get('/checktwitchstatus', [
    'uses'      => 'EventController@checkTwitchStatus',
  ]);

  Route::get('/twitch', [
    'uses'      => 'EventController@twitchapi',
  ]);
  Route::get('/events/', [
    'as'        => 'events.index',
    'uses'      => 'EventController@index',
  ]);

  Route::get('/events/{id}', [
    'as'        => 'events.show',
    'uses'      => 'EventController@show',
  ]);

  Route::get('/events/{id}/destroy', [
    'as'        => 'events.destroy',
    'uses'      => 'EventController@destroy',
  ]);

  Route::get('/events/{id}/edit', [
    'as'        => 'events.edit',
    'uses'      => 'EventController@edit',
  ]);

  Route::post('/events/update/', [
    'as'        => 'updateEvent',
    'uses'      => 'EventController@update',
  ]);
  Route::put('/events/update/', [
    'as'        => 'updateEvent',
    'uses'      => 'EventController@update',
  ]);

    Route::get('/ajaxEvents', 'EventController@ajaxEvents')->name('ajaxEvents');

    Route::get('/events/{id}/publish', [
      'as'        => 'publishEvent',
      'uses'      => 'EventController@publish',
    ]);
});


Auth::routes([
    'register' => false
]);
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
